﻿using System;
using BusinessObject;
using Services;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMSEngine.DataBaseGateway;
using Services.DataAccess;
using System.Data.SqlClient;

namespace Services.Service
{
    public class UserService : CommonGateway, IUser
    {
        #region Maping


        #endregion
        private User MapObject(SqlDataReader oReader)
        {
            User oUser = new User();
            oUser.UserID = (int)oReader["UserID"];
            oUser.UserName = oReader["UserName"].ToString();
            oUser.Password = oReader["Password"].ToString();
            oUser.Email = oReader["Email"].ToString();
            oUser.UserType = (int)oReader["UserType"];
            return oUser;
        }
        private User MakeObject(SqlDataReader oReader)
        {
            oReader.Read();
            User oUser = new User();
            oUser = MapObject(oReader);
            return oUser;
        }
        private List<User> MakeObjects(SqlDataReader oReader)
        {
            List<User> oUsers = new List<User>();
            while (oReader.Read())
            {
                User oUser = MapObject(oReader);
                oUsers.Add(oUser);
            }
            return oUsers;
        }

        #region Function Implementation
        public User ValidateLogin(int nBUID, User oUser)
        {
            Connection.Open();
            Command.CommandText = UserDA.ValidateLogin(nBUID, oUser);

            SqlDataReader reader = Command.ExecuteReader();
            User _oUser = new User();
            if(reader.HasRows)
            {
                _oUser = MakeObject(reader);
            }
            reader.Close();
            Connection.Close();
            return _oUser;
        }
        public List<User> Gets(int nBUID, int nUser)
        {
            Connection.Open();
            Command.CommandText = UserDA.Gets(nBUID, nUser);

            SqlDataReader reader = Command.ExecuteReader();
            User _oUser = new User();
            List<User> _oUsers = new List<User>();
            if (reader.HasRows)
            {
                _oUsers = MakeObjects(reader);
            }
            reader.Close();
            Connection.Close();
            return _oUsers;
        }
        #endregion

    }
}
