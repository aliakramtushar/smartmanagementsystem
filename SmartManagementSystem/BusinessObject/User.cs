﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject
{
    public class User
    {
        public User()
        {
            UserID = 0;
            UserName = "";
            Password = "";
            UserType = 0;
            Email = "";
            Validity = true;
            ErrorMessage = "";
        }
        #region Property
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int UserType { get; set; }
        public string Email { get; set; }
        public bool Validity { get; set; }
        public bool Activity { get; set; }
        public string ErrorMessage { get; set; }
        #endregion

    }
    public interface IUser
    {
        User ValidateLogin(int nBUID, User oUser);
        List<User> Gets(int nBUID, int nUser);
    }


}
