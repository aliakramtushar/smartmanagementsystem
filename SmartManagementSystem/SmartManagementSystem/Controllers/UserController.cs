﻿using BusinessObject;
using SMSEngine;
using Services.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMSEngine.GlobalClass;

namespace SmartManagementSystem.Controllers
{
    public class UserController : Controller
    {
        User _oUser = new User();
        List<User> _oUsers = new List<User>() { };
        UserService _oUserService = new UserService();
        public ActionResult ViewUsers(int nBUID, int nUserID)
        {
            ViewBag.BUID = nBUID;
            ViewBag.UserID = nUserID;
            _oUsers = _oUserService.Gets(nBUID, nUserID);
            return View();
        }
        public ActionResult ViewUser()
        {
            return View();
        }
    }
}